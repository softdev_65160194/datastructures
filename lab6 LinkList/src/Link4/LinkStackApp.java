package Link4;

class LinkStack
{
private LinkList theList;
//--------------------------------------------------------------
public LinkStack() // constructor
{
theList = new LinkList();
}
//--------------------------------------------------------------
public void push(long j) // put item on top of stack
{
theList.insertFirst(j);
}
//--------------------------------------------------------------
public long pop() // take item from top of stack
{
return theList.deleteFirst();
}
//--------------------------------------------------------------
public boolean isEmpty() // true if stack is empty
{
return ( theList.isEmpty() );
}
//--------------------------------------------------------------
public void displayStack()
{
System.out.print("Stack (top-->bottom): ");
theList.displayList();
}
//--------------------------------------------------------------
} // end class LinkStack
////////////////////////////////////////////////////////////////
class LinkStackApp
{
public static void main(String[] args)
{
LinkStack theStack = new LinkStack(); // make stack
theStack.push(20); // push items
theStack.push(40);
theStack.displayStack(); // display stack
theStack.push(60); // push items
theStack.push(80);
theStack.displayStack(); // display stack
theStack.pop(); // pop items
theStack.pop();
theStack.displayStack(); // display stack
} // end main()
} // end class LinkStackApp
////////////////////////////////////////////////////////////////

import java.util.Arrays;

public class ArrayManipulationlab1t2 {
    static int[] arr = {1, 0, 2, 3, 0, 4, 5, 0};

    public static void main(String[] args) {
        for (int i = arr.length - 2; i >= 0; i--) { // Corrected loop condition (changed i++ to i--)
            if (arr[i] == 0) {
                for (int count = arr.length - 2; count > i; count--) {
                    arr[count + 1] = arr[count];
                }
                arr[i + 1] = 0;
            }
        }
        System.out.println(Arrays.toString(arr));
    }
}
